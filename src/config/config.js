/**
 * Created by lonelydawn on 2018/8/20$.
 */

export default {
  announcements: [{
    id: 0,
    name: '01',
    time: '2022/4/16',
    title: '年轻人一定要警惕超前消费的陷阱！',
    detail: '随着消费信贷的迅速发展，我们时常会听到“超前消费”这个词，这种消费理念也是当下年轻人中，非常推崇和认可的消费方式。他们对物质生活有更高的追求，对新鲜事物有着较强的接纳性，可是缺乏必要的自控能力，加上新闻和舆论相关的报道，导致对超前消费产生了比较片面的解读。大学生超前消费并不是一个新话题，早在2009年，央行为防止大学生在求学期间背上沉重的债务，就叫停了银行对大学生发放透支额在1000元以上的信用卡，其保护学生、限制过度消费的意图非常明显。然而，由于大学生超前消费需求旺盛，大量面向在校学生的不良信贷平台如雨后春笋般出现。同时，花呗等正规信用贷平台在高校学生中的渗透率也越来越高。鉴于此，教育部要求各地各高校集中开展校园不良网贷风险警示教育工作就显得尤为重要。因为大学生超前消费，背后是消费观念出了问题，对过度消费和从众消费等错误观念茫然无知，对网贷风险理解和认识不够，对不良网贷的甄别抵制能力不强，从而容易将自己暴露在金融风险当中。其实，超前消费本身是一个中性词。合理的超前消费可以转化为动力和压力，激励我们努力赚钱，追求更好的生活。但是，如果过度消费，缺乏对自己能力的认知，不能制定合理的收支计划，追求远远超出自己所能承受的风险，就变成消耗你的人生，透支生活。高消费，也许会带来短暂的激情，也许会迎来别人羡慕的目光，内心获得了前所未有的满足感。但是，这样的优越感并不长久，反而会带来长久的痛苦。想要真正获得别人和社会的认可，并不是因为你穿的什么，用的什么。而是通过你自身的努力和付出获得财富和自由！赢得别人的尊重和社会的认可！最后，还是提醒正在超前消费的年轻人，一定要警惕超前消费的陷阱，不要提前透支自己的未来！', cover: './static/images/decorates/talk1.png'
  },
  {
    id: 1,
    name: '02',
    title: '公告2',
    time: '2022/4/20',
    detail: '公告2详情',
    cover: './static/images/decorates/talk1.png'
  },
  {
    id: 2,
    name: '03',
    title: '公告3',
    time: '2022/5/6',
    detail: '公告3详情',
    cover: './static/images/decorates/talk1.png'
  },
  {
    id: 3,
    name: '04',
    title: '公告4',
    time: '2022/5/10',
    detail: '公告4详情',
    cover: './static/images/decorates/talk1.png'
  },
  {
    id: 4,
    name: '05',
    title: '公告5',
    time: '2022/5/20',
    detail: '公告5详情',
    cover: './static/images/decorates/talk1.png'
  },
  {
    id: 4,
    name: '06',
    title: '公告6',
    time: '2022/5/20',
    detail: '公告6详情',
    cover: './static/images/topics/topic-6.png'
  },
  ],

  goods: [{
    id: 0,
    name: '01',
    text: '波西米亚编织星月捕梦网家居装修壁挂手工编织',
    address: '天津科技大学',
    type: '包邮',
    price: '39.80',
    onlinePrice: '29.80',
    cover: require('../../static/images/dormitory/main-01.jpg'),
    poster: require('../../static/images/dormitory/detail-01.jpg'),
    color: '#96520c',
    detail: '波西米亚编织星月捕梦网 家居装修壁挂手工编织, 包邮 包邮 包邮, 拍下不带小灯泡, 两款, 具体款式看图片上的标签A1 A2 A3 A4 A5。A款单个 29.8, B1 B2 B3 B4 B5  B款长约 80cm, 点击立即购买。',
    images: [
      '../../static/images/dormitory/main-01.jpg'
    ]
  },
  {
    id: 1,
    name: '02',
    text: '简易晾衣架落地立式卧室挂衣服架子加粗小型阳台家用',
    address: '北京大学',
    type: '包邮',
    price: '19.90',
    onlinePrice: '9.90',
    cover: './static/images/dormitory/main-02.jpg',
    poster: './static/images/dormitory/detail-02.jpg',
    color: '#96520c',
    detail: '【颜色】黑 白，规格与价格见详情。点我想要-立即购买，可选择规格与价格. 质量很好，颜值高的一款落地衣架，阳台，卧室，宿舍都方便用，双层晾晒被子也是可以的。',
    images: [
      './static/images/dormitory/main-02.jpg'
    ]
  },
  {
    id: 2,
    name: 'BBB',
    text: '化妆品收纳盒壁挂式免打孔防尘',
    address: '天津大学',
    type: '包邮',
    price: '60.00',
    onlinePrice: '40.50',
    cover: './static/images/dormitory/main-03.jpg',
    poster: './static/images/dormitory/detail-03.jpg',
    color: '#96520c',
    detail: '化妆品收纳盒壁挂式免打孔防尘，家用大容量卫生间挂墙上，浴室置物架【包邮】低价处理,可小刀。',
    images: [
      './static/images/dormitory/main-03.jpg'
    ]
  },
  {
    id: 3,
    name: '04',
    text: 'Patagonia巴塔哥尼亚户外马克杯咖啡杯简约印花',
    address: '上海交通大学',
    type: '包邮',
    price: '55.60',
    onlinePrice: '38.80',
    cover: './static/images/dormitory/main-04.jpg',
    poster: './static/images/dormitory/detail-04.jpg',
    color: '#96520c',
    detail: 'Patagonia巴塔哥尼亚户外马克杯咖啡杯简约印花，物美价廉 耐用 不撞杯，杯身有下图标记出的小点，类似杯子制作过程中粘到的颜料，看不怎么出来，如果用白纸垫在下面会看到，介意者慎拍。',
    images: [
      './static/images/dormitory/main-04.jpg'
    ]
  },
  {
    id: 4,
    name: '05',
    text: '宜家置物架小推车多层收纳架带轮储物架子',
    address: '清华大学',
    type: '包邮',
    price: '35.00',
    onlinePrice: '28.00',
    cover: './static/images/dormitory/main-05.jpg',
    poster: './static/images/dormitory/detail-05.jpg',
    color: '#96520c',
    detail: '宜家同款网红手推车置物架，八分新，带把手，加厚碳钢金属材材质，静音万向轮带刹车可固定。美容院，工作室，美甲店，客厅厨房书房等都可用。',
    images: [
      './static/images/dormitory/main-05.jpg'
    ]
  },
  {
    id: 5,
    name: '06',
    text: '可叠加抽屉式桌面收纳盒ins少女',
    address: '天津科技大学',
    type: '包邮',
    price: '15.00',
    onlinePrice: '8.80',
    cover: './static/images/dormitory/main-06.jpg',
    poster: './static/images/dormitory/detail-06.jpg',
    color: '#96520c',
    detail: '可叠加抽屉式桌面收纳盒，ins少女心桌面学生宿舍神器储物盒，办公室书桌好看还实用的收纳盒非它了✔️马卡龙莫兰迪色系，简直不要太好看 质量很好 容量也很大',
    images: [
      './static/images/dormitory/main-06.jpg'
    ]
  },
  {
    id: 6,
    name: '07',
    text: '硅藻泥脚垫 迎虎年软硅藻',
    address: '南开大学',
    type: '不包邮',
    price: '19.90',
    onlinePrice: '9.9',
    cover: './static/images/dormitory/main-07.jpg',
    poster: './static/images/dormitory/detail-07.jpg',
    color: '#96520c',
    detail: '用过两次，九成新，硅藻泥 吸水性强 厚度0.5厘米，不包邮，支持同城自取。可自助下单 点击我想要 点立即购买拍下，48小时内发货，全国包邮，偏远地区除外！',
    images: [
      './static/images/dormitory/main-07.jpg'
    ]
  },
  {
    id: 7,
    name: '08',
    text: '三个元杂物收纳盒 桌面塑料盒子浴室化妆品整理盒',
    address: '南开大学',
    type: '包邮',
    price: '15.80',
    onlinePrice: '10.8',
    cover: './static/images/dormitory/main-08.jpg',
    poster: './static/images/dormitory/detail-08.jpg',
    color: '#96520c',
    detail: '全新！包邮！包邮！三个元杂物收纳盒，桌面塑料盒子浴室化妆品整理盒储物盒零食收纳筐子。质量非常好厚实，可以用于厨房收纳，冰箱收纳，护肤品收纳，卫生间收纳，台面收纳，书本收纳，零食收纳等等，用处广泛买到就是赚到！需要的可以直接拍下，拍下发默认款！',
    images: [
      './static/images/dormitory/main-08.jpg'
    ]
  }
  ]
}
