import Vue from 'vue'
import VueRouter from 'vue-router'
import Index from '@/components/Index'
import Goods from '@/components/Goods'
import Order from '@/components/Order'
import Car from '@/components/Car'
import Login from '@/components/Login'
import Goodlist from '@/components/Goodlist'
import Discount from '@/components/Discount'
import Contact from '@/components/Contact'
import Publish from '@/components/Publish'
import MyGoods from '@/components/MyGoods'
// import Footer from '@/components/Footer'
import Topic from '@/components/Topic'

Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        name: 'Index',
        component: Index
    },
    {
        path: '/goods',
        name: 'Goods',
        component: Goods
    },
    {
        path: '/mygoods',
        name: 'MyGoods',
        component: MyGoods
    },
    {
        path: '/order',
        name: 'Order',
        component: Order
    },
    {
        path: '/car',
        name: 'Car',
        component: Car
    },
    {
        path: '/Login',
        name: 'Login',
        component: Login
    },
    {
        path: '/Goodlist',
        name: 'Goodlist',
        component: Goodlist
    },
    {
        path: '/Discount',
        name: 'Discount',
        component: Discount
    },
    {
        path: '/Contact',
        name: 'Contact',
        component: Contact
    },
    {
        path: '/Publish',
        name: 'Publish',
        component: Publish
    },
    {
        path: '/Topic',
        name: 'Topic',
        component: Topic
    }
]

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
})

const originalPush = VueRouter.prototype.push;
VueRouter.prototype.push = function push(location) {
    return originalPush.call(this, location).catch(err => err)
}

export default router