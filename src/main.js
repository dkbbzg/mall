import Vue from 'vue'
import './plugins/axios'
import App from './App.vue'
import router from './router'
import store from './store'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import 'font-awesome/css/font-awesome.css'
import '@/assets/common.css'
import VueAwesomeSwiper from 'vue-awesome-swiper'


Vue.use(VueAwesomeSwiper)
Vue.use(ElementUI);
Vue.config.productionTip = false

let this_vue = new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')

export default this_vue
