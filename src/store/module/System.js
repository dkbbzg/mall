export default {
    namespaced: 'System',
    state: {
        UserInfo: {},   //  用户信息
    },
    mutations: {
        //  用户信息
        UserInfoChange(state, params) {
            state.UserInfo = params;
        },
    },
    actions: {},
    modules: {}
}