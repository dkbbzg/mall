import base from './base';
import axios from '../utils/axios_http';
import md5 from 'js-md5'

const User = {
  getUserInfo() {
    return axios.post(`${base.User}/getUserInfo`)
  },
  login(params) {
    return axios.post(`${base.User}/login`, {
      user: params.username,
      pwd: md5(params.password)
    })
  },
  logout() {
    return axios.post(`${base.User}/logout`)
  },
  register(params) {
    return axios.post(`${base.User}/register`, {
      user: params.username,
      pwd: md5(params.password),
    })
  },
};

export default User;