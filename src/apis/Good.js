import base from './base';
import axios from '../utils/axios_http';

const Goods = {
  // 获取表格数据
  getGoods(params) {
    return axios.post(`${base.Goods}/getGoods`, params);
  },
  // 删除弹出框点击确定操作
  deleteGoods(params) {
    return axios.post(`${base.Goods}/deleteGoods`, params);
  },
  // 新增编辑弹出框点击确定操作
  addEditGoods(params) {
    return axios.post(`${base.Goods}/addEditGoods`, params);
  },

  // 新增
  addGoods(params) {
    return axios.post(`${base.Goods}/addGoods`, params);
  },
  // 我的商品
  getMyGoods(params) {
    return axios.post(`${base.Goods}/getMyGoods`, params);
  },
  // 我的商品 删除
  deleteMyGood(params) {
    return axios.post(`${base.Goods}/deleteMyGood`, params);
  },
};

export default Goods;